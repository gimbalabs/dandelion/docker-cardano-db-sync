FROM inputoutput/cardano-db-sync:13.0.5 AS final
COPY --from=busybox /bin/tar /bin/tar
COPY --from=busybox /bin/gzip /bin/gzip
COPY --from=busybox /bin/grep /bin/grep
COPY --from=stedolan/jq /usr/local/bin/jq /bin/jq
